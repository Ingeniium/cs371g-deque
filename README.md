# CS371g: Generic Programming Deque Repo

* Name: Andrew Biriko Olumbe

* EID: abo396

* GitLab ID:  Ingeniium

* Git SHA: d8630d38afa2921e333a114f9ad0c1e673c0ebf1

* GitLab Pipelines: https://gitlab.com/Ingeniium/cs371g-deque/-/pipelines

* Estimated completion time: 25

* Actual completion time: 40

* Comments: N/A
