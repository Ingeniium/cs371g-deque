// -------
// Deque.h
// --------

#ifndef Deque_h
#define Deque_h

// --------
// includes
// --------

#include <algorithm>        // copy, equal, lexicographical_compare, max, swap
#include <cassert>          // assert
#include <initializer_list> // initializer_list
#include <iterator>         // bidirectional_iterator_tag
#include <memory>           // allocator
#include <stdexcept>        // out_of_range
#include <utility>          // !=, <=, >, >=

// -----
// using
// -----

using std::rel_ops::operator!=;
using std::rel_ops::operator<=;
using std::rel_ops::operator>;
using std::rel_ops::operator>=;
using namespace std;
// -------
// destroy
// -------

template <typename A, typename BI>
BI my_destroy (A& a, BI b, BI e) {
    while (b != e) {
        --e;
        a.destroy(&*e);
    }
    return b;
}

// ------------------
// uninitialized_copy
// ------------------

template <typename A, typename II, typename BI>
BI my_uninitialized_copy (A& a, II b, II e, BI x) {
    BI p = x;
    try {
        while (b != e) {
            a.construct(&*x, *b);
            ++b;
            ++x;
        }
    }
    catch (...) {
        my_destroy(a, p, x);
        throw;
    }
    return x;
}

// ------------------
// uninitialized_fill
// ------------------

template <typename A, typename BI, typename T>
void my_uninitialized_fill (A& a, BI b, BI e, const T& v) {
    BI p = b;
    try {
        while (b != e) {
            a.construct(&*b, v);
            ++b;
        }
    }
    catch (...) {
        my_destroy(a, p, b);
        throw;
    }
}

// --------
// my_deque
// --------

template <typename T, typename A = std::allocator<T>>
class my_deque {
    // -----------
    // operator ==
    // -----------

    /**
     * Checks whether two deques are equal
     */
    friend bool operator == (const my_deque& lhs, const my_deque& rhs) {
        return equal(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
    }

    // ----------
    // operator <
    // ----------

    /**
     * Does a lexicographical comparison of two deques
     */
    friend bool operator < (const my_deque& lhs, const my_deque& rhs) {
        return lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
    }

    // ----
    // swap
    // ----

    /**
     * Swaps the contents of two deques
     */
    friend void swap (my_deque& x, my_deque& y) {
        x.swap(y);
    }

public:
    // ------
    // usings
    // ------

    // you must use this allocator for the inner arrays
    using allocator_type  = A;
    using value_type      = typename allocator_type::value_type;

    using size_type       = typename allocator_type::size_type;
    using difference_type = typename allocator_type::difference_type;

    using pointer         = typename allocator_type::pointer;
    using const_pointer   = typename allocator_type::const_pointer;

    using reference       = typename allocator_type::reference;
    using const_reference = typename allocator_type::const_reference;

    // you must use this allocator for the outer array
    using allocator_type_2 = typename A::template rebind<pointer>::other;

private:
    // ----
    // data
    // ----

    allocator_type _a;
    static const int ele_per_bucket = 21;// Number of elements per bucket. COde works when this is > 2
    size_t num_buckets = 3; //Number of buckets the deque has
    size_t _size = 0;// Number of constructed elements in this deque
    T** buckets = nullptr; //Pointers to the buckets holding the elements
    T** center_bucket = nullptr; //Pointer pointing to the pointer in the buckets ptr arr of the center most bucket.
    T** front_bucket = nullptr;//Pointer to the front most bucket.
    T** back_bucket = nullptr; //Pointer to the back most bucket.

private:
    // -----ne
    // valid
    // -----

    bool valid () const {

        return back_bucket >= front_bucket && _size >= 0 && center_bucket >= front_bucket;
    }

public:
    // --------
    // iterator
    // --------

    class iterator {
        // -----------
        // operator ==
        // -----------

        /**
         * Checks if two iterators are equal
         */
        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            return rhs.deque == lhs.deque && rhs.icenter == lhs.icenter && rhs.index == lhs.index;
        }

        /**
         * Checks if two iterators are unequal
         */
        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

        // ----------
        // operator +
        // ----------

        /**
         * Gets an iterator that is ahead of param one
         @param an iterator of the deque
         @param some integer indicating how much places forward in the deque to refer to
         */

        friend iterator operator + (iterator lhs, difference_type rhs) {
            return lhs += rhs;
        }

        // ----------
        // operator -
        // ----------

        /**
        * Gets an iterator that is behind of param one
        @param an iterator of the deque
        @param some integer indicating how much places behind in the deque to refer to
        */
        friend iterator operator - (iterator lhs, difference_type rhs) {
            return lhs -= rhs;
        }

    public:
        // ------
        // usings
        // ------

        // this requires a weaker iterator than the real deque provides
        using iterator_category = std::bidirectional_iterator_tag;
        using value_type        = typename my_deque::value_type;
        using difference_type   = typename my_deque::difference_type;
        using pointer           = typename my_deque::pointer;
        using reference         = typename my_deque::reference;

    public:
        // ----
        // data
        // ----
        my_deque* deque;
        T* element; // ptr to the element they are pointing to.
        int icenter; //how far away this iter's bucket is from the center bucket. Exists to work in liue of bucket ptr reallocations.
        int index; //index of the element in the bucket
    private:
        // -----
        // valid
        // -----

        bool valid () const {
            // <your code>
            return true;
        }

    public:
        // -----------
        // constructor
        // -----------

        /**
         * your documentation
         */
        iterator (T* _element, size_type _icenter, int _index, my_deque* _deque)
            : element(_element), icenter(_icenter), index(_index), deque(_deque) { }

        iterator             () = default;
        iterator             (const iterator&) = default;
        ~iterator            ()                = default;
        iterator& operator = (const iterator&) = default;


        // ----------
        // operator *
        // ----------

        /**
         * Gets a reference to the element pointed to
         */
        reference operator * () const {
            return *element;
        }

        // -----------
        // operator ->
        // -----------

        /**
         * your documentation
         */
        pointer operator -> () const {
            return &**this;
        }

        // -----------
        // operator ++
        // -----------

        /**
         * Makes this iterator reference the element ahead 1 place in the deque
         */
        iterator& operator ++ () {
            assert(valid());
            return *this += 1;
        }

        /**
         * Makes this iterator reference the element ahead 1 place in the deque
           But returns its previous value.
         */
        iterator operator ++ (int) {
            iterator x = *this;
            ++(*this);
            assert(valid());
            return x;
        }

        // -----------
        // operator --
        // -----------

        /**
         * Makes this iterator reference the element behind 1 place in the deque
         */
        iterator& operator -- () {

            return *this -= 1;
        }

        /**
         * Makes this iterator reference the element behind 1 place in the deque
           But returns its previous value.
         */
        iterator operator -- (int) {
            iterator x = *this;
            --(*this);
            assert(valid());
            return x;
        }

        // -----------
        // operator +=
        // -----------

        /**
         * Makes this iterator reference the element ahead param places in the deque
           @param number of places ahead to refer to
         */
        //This is basically done the opposite of -=
        iterator& operator += (difference_type d) {
            if(index + d >= deque->ele_per_bucket)
            {
                if(*deque->back_bucket + deque->ele_per_bucket - 1 != element)
                {
                    icenter += (index + d) / deque->ele_per_bucket;
                    index = (index + d) % deque->ele_per_bucket;

                }
                else //If the bucket is the backmost in the deque, set index to -1 and return
                {
                    index = -1;
                    //cout << " Index now " << index << " of bucket " << icenter << endl;
                    return *this;
                }
            }
            else//Otherwise, just increment the index
            {
                index += d;
            }
            element = *(deque->center_bucket + icenter) + index;

            assert(valid());
            return *this;
        }

        // -----------
        // operator -=
        // -----------

        /**
         * Makes this iterator reference the element behind param places in the deque
           @param number of places behind to refer to
         */
        iterator& operator -= (difference_type d) {
            //  cout << "Decrementing pointer!";
            /* If the iterator would be front of the bucket,
            adjust icenter index and make it point to the last
            element of the bucket in front of the last one.*/
            //cout << (index - (int)d) / deque->ele_per_bucket << endl;
            if(index - d < 0)
            {
                assert(d > index);
                //cout << index - d << endl;
                if(*deque->front_bucket != element)
                {

                    index -= d;

                    --icenter;
                    for(; index < -1 * deque->ele_per_bucket; index += deque->ele_per_bucket)
                    {
                        --icenter;
                    }
                    index = deque->ele_per_bucket + index;
                }
                else //If the bucket is the frontmost in the deque, set index to -1 and return
                {
                    index = -1;
                    //cout << " Index now " << index << endl;
                    return *this;
                }
            }
            else//Otherwise, just decrement the index
            {
                index -= d;
            }
            element = *(deque->center_bucket + icenter) + index;
            assert(valid());
            return *this;
        }
    };
    iterator front_it; //Iterator pointing to the frontmost element within the front bucket
    iterator back_it; //Iterator pointing to the back most element in the back bucket.

public:

    // --------------
    // const_iterator
    // --------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        /**
         * your documentation
         */
        friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
            return rhs.deque == lhs.deque && rhs.icenter == lhs.icenter && rhs.index == lhs.index;
        }

        /**
         * your documentation
         */
        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

        // ----------
        // operator +
        // ----------

        /**
         * your documentation
         */
        friend const_iterator operator + (const_iterator lhs, difference_type rhs) {
            return lhs += rhs;
        }

        // ----------
        // operator -
        // ----------

        /**
         * your documentation
         */
        friend const_iterator operator - (const_iterator lhs, difference_type rhs) {
            return lhs -= rhs;
        }

    public:
        // ------
        // usings
        // ------

        // this requires a weaker iterator than the real deque provides
        using iterator_category = std::bidirectional_iterator_tag;
        using value_type        = typename my_deque::value_type;
        using difference_type   = typename my_deque::difference_type;
        using pointer           = typename my_deque::const_pointer;
        using reference         = typename my_deque::const_reference;

    public:
        // ----
        // data
        // ----
        my_deque* deque;
        const T* element; // ptr to the element they are pointing to.
        int icenter; //how far away this iter's bucket is from the center bucket. Exists to work in liue of bucket ptr reallocations.
        int index; //index of the element in the bucket
    private:
        // -----
        // valid
        // -----

        bool valid () const {
            // <your code>
            return true;
        }

    public:
        // -----------
        // constructor
        // -----------

        /**
         * your documentation
         */
        const_iterator (T* _element, size_type _icenter, int _index, my_deque* _deque)
            : element(_element), icenter(_icenter), index(_index), deque(_deque) { }

        const_iterator(const iterator& rhs) : element(rhs.element), icenter(rhs.icenter), index(rhs.index), deque(rhs.deque)
        {}

        const_iterator             () = default;
        const_iterator             (const_iterator&) = default;
        ~const_iterator            ()                = default;
        const_iterator& operator = (const_iterator&) = default;


        // ----------
        // operator *
        // ----------


        reference operator * () const {
            return *element;
        }

        // -----------
        // operator ->
        // -----------


        pointer operator -> () const {
            return &**this;
        }

        // -----------
        // operator ++
        // -----------


        const_iterator& operator ++ () {
            assert(valid());
            return *this += 1;
        }


        const_iterator operator ++ (int) {
            iterator x = *this;
            ++(*this);
            assert(valid());
            return x;
        }

        // -----------
        // operator --
        // -----------

        const_iterator& operator -- () {

            return *this -= 1;
        }


        const_iterator operator -- (int) {
            iterator x = *this;
            --(*this);
            assert(valid());
            return x;
        }

        // -----------
        // operator +=
        // -----------


        //This is basically done the opposite of -=
        const_iterator& operator += (difference_type d) {
            if(index + d >= deque->ele_per_bucket)
            {
                if(*deque->back_bucket + deque->ele_per_bucket - 1 != element)
                {
                    icenter += (index + d) / deque->ele_per_bucket;

                    index = (index + d) % deque->ele_per_bucket;

                }
                else //If the bucket is the backmost in the deque, set index to -1 and return
                {
                    index = -1;
                    //cout << " Index now " << index << " of bucket " << icenter << endl;
                    return *this;
                }
            }
            else//Otherwise, just increment the index
            {
                index += d;
            }
            element = *(deque->center_bucket + icenter) + index;

            assert(valid());
            return *this;
        }

        // -----------
        // operator -=
        // -----------


        const_iterator& operator -= (difference_type d) {
            //cout << "Decrementing pointer!";
            /* If the iterator would be front of the bucket,
            adjust icenter index and make it point to the last
            element of the bucket in front of the last one.*/
            //cout << (index - (int)d) / deque->ele_per_bucket << endl;
            if(index - d < 0)
            {
                assert(d > index);
                //cout << index - d << endl;
                if(*deque->front_bucket != element)
                {

                    index -= d;

                    --icenter;
                    for(; index < -1 * deque->ele_per_bucket; index += deque->ele_per_bucket)
                    {
                        --icenter;
                    }
                    index = deque->ele_per_bucket + index;

                }
                else //If the bucket is the frontmost in the deque, set index to -1 and return
                {
                    index = -1;
                    // cout << " Index now " << index << endl;
                    return *this;
                }
            }
            else//Otherwise, just decrement the index
            {
                index -= d;
            }
            element = *(deque->center_bucket + icenter) + index;

            assert(valid());
            return *this;
        }
    };

public:
    // ------------
    // constructors
    // ------------

    my_deque ()
    {
        buckets = new T*[num_buckets];
        /* With a default deque, the front and back
        will be pointing to the same element within the
        same bucket. The bucket pointed to will be in the
        middle */
        memset(buckets, 0, num_buckets * sizeof(T*));//Set non allocated bucket ptrs to nullptr
        /* Now allocate space  for the center bucket ... */
        T* bucket = _a.allocate(ele_per_bucket);
        buckets[num_buckets / 2] = bucket;

        front_bucket = buckets + num_buckets / 2;
        back_bucket = front_bucket;
        center_bucket = front_bucket;

        front_it = iterator(*front_bucket + ele_per_bucket / 2, 0, ele_per_bucket / 2, this);
        back_it = front_it;


    }

    /**
     * Creates a deque with param elements
     @param number of elements to instantiate deque with
     */
    explicit my_deque (size_type s) : my_deque(s, value_type()) { }


    /**
     * Creates a deque with param elements, setting them to a specific value
       @param number of elements to instantiate deque with
       @param value of elements to instantiate deque with
     */
    my_deque (size_type s, const_reference v) : my_deque() {
        for(size_type i = 0; i < s; ++i)
            push_back(v);
        assert(valid());
    }

    /**
     * Creates a deque with param elements, setting them to a specific value
        and with a specific allocator
       @param number of elements to instantiate deque with
       @param value of elements to instantiate deque with
       @param copy of an allocator to use
     */
    my_deque (size_type s, const_reference v, const allocator_type& a) : my_deque(s, v) {
        _a = a;
    }

    /**
     * Creates a deque with elements in the initializer list
       @param list of elements to copy
     */
    my_deque (std::initializer_list<value_type> rhs) : my_deque() {
        auto it = rhs.begin();
        while(it != rhs.end())
        {
            push_back(*it);
            ++it;
        }
        assert(valid());
    }

    /**
     * Creates a deque with elements in the initializer list and with a specific allocator
       @param list of elements to copy
       @param allocator to copy
     */
    my_deque (std::initializer_list<value_type> rhs, const allocator_type& a) : my_deque(rhs) {
        _a = a;
    }

    /**
     * Instantiates this deque wih the elements of another deque
     */
    my_deque (const my_deque& that) : my_deque() {
        for(size_type i = 0; i < that.size(); ++i)
            push_back(that[i]);
    }

    // ----------
    // destructor
    // ----------

    /**
     * Destroys the deque
     */
    ~my_deque () {
        clear();
        if(buckets != nullptr)//Get rid of bucket ptrs
            delete[] buckets;
        assert(valid());
    }

    // ----------
    // operator =
    // ----------

    /**
     * Copies the elements of another deque
     */
    my_deque& operator = (const my_deque& rhs) {
        size_type s = size();
        for(size_type i = 0; i < rhs.size(); ++i)
        {
            if(i < s)
                at(i) = rhs[i];
            else
                push_back(rhs[i]);
        }
        return *this;
    }

    // -----------
    // operator []
    // -----------

    /**
     * Returns a reference to the element at param position
       @param index of element to reference
     */
    reference operator [] (size_type index) {
        /* We can pretend that the elements in the deque start at
        the frontmost bucket by adding the index with ele_per_bucket
        for each bucket that can be supported by the current bucket ptrs
        array that would be ahead of the current front bucket had they
        been allocated */
        index += (front_bucket - buckets) * ele_per_bucket;
        /* We can also make further adjustmest to the index to account
        for the frontmost element not being at the front of the bucket. */
        index += max(front_it.index, 0);

        size_type bucket_index = index / ele_per_bucket;
        T* bucket = buckets[bucket_index];

        index = index % ele_per_bucket;
        //cout << "Accessing bucket " << bucket_index << " out of " << num_buckets << " possible buckets at index " << index << endl;
        return bucket[index];
    }           //

    /**
     * Returns a reference to the element at param position
       @param index of element to reference
     */
    const_reference operator [] (size_type index) const {
        return const_cast<my_deque*>(this)->operator[](index);
    }

    // --
    // at
    // --

    /**
     * Returns a reference to the element at param position
       @param index of element to reference
     * @throws out_of_range
     */
    reference at (size_type index) {
        /* Same idea as above, but need to check if the
        index would point to an element past the supported
        range.*/

        if(index >= _size)
            throw std::out_of_range("Out of range!");
        return operator[](index);
    }

    /**
     * Returns a readonly reference to the element at param position
       @param index of element to reference
     * @throws out_of_range
     */
    const_reference at (size_type index) const {
        return const_cast<my_deque*>(this)->at(index);
    }

    // ----
    // back
    // ----

    /**
     * Returns a reference of the backmost element
     */
    reference back () {
        //cout << "back ()" << endl;
        return *(back_it - 1);
    }

    /**
     * Returns a readonly reference of the backmost element
     */
    const_reference back () const {
        return const_cast<my_deque*>(this)->back();
    }

    // -----
    // begin
    // -----

    /**
     * Returns an iterator pointing to the frontmost element.
     */
    iterator begin () {
        return front_it;
    }

    /**
     * Returns a readonly iterator pointing to the frontmost element.
     */
    const_iterator begin () const {
        return const_iterator(front_it);
    }

    // -----
    // clear
    // -----

    /**
     * Removes all elements from the deque.
     */
    void clear () { //Simply do a series of pop_backs
        while(!empty())
            pop_back();
        assert(valid());
    }

    // -----
    // empty
    // -----

    /**
     * Returns whether he deque has no elements.
     */
    bool empty () const {
        return !size();
    }

    // ---
    // end
    // ---

    /**
     * Returns an iterator pointing to the backmost element.
     */
    iterator end () {
        return back_it;
    }

    /**
     * Returns a readonly iterator pointing to the backmost element.
     */
    const_iterator end () const {
        return const_iterator(back_it);
    }


    // -----
    // front
    // -----

    /**
     * Returns a reference of the frontmost element
     */
    reference front () {

        return *front_it;
    }

    /**
     * Returns a readonly reference of the frontmost element.
     */
    const_reference front () const {
        return const_cast<my_deque*>(this)->front();
    }

    // ------
    // insert
    // ------

    /**
     * Inserts a value to a place in the deque pointed to
       by an iterator. This increases deque size by one.
       @param iterator of the deque
       @param value to insert to the deque
     */
    iterator insert (iterator it, const_reference v) {
        //If it is begin() or end(), just do a normal push
        if(it == front_it)
            push_front(v);
        else if(it == back_it)
            push_back(v);
        else//If inserting in the middle, set content and shift everything back. (by element)
        {
            push_back(at(size() - 1));
            for(iterator i = back_it - 1; i != it;  --i)
                *i = *(i - 1);
            *it = v;
        }
        assert(valid());
        return it;
    }

    // ---
    // pop
    // ---

    /**
     * Removes the backmost element of the deque, reducing the size by one.
     */
    void pop_back () {
        //cout << "Getting pointer..." <<endl;
        iterator it = back_it - 1;
        // cout << "Destroying index " << it.index << " of bucket " << it.icenter << endl;
        _a.destroy(&*it);
        --_size;
        back_it = it;
        back_bucket = center_bucket + back_it.icenter;
        assert(valid());
    }

    /**
     * Removes the frontmost element of the deque, reducing the size by one.
     */
    void pop_front () {
        //cout << "Destroying index " << front_it.index << " of bucket " << front_it.icenter << endl;
        _a.destroy(&*front_it);
        --_size;
        ++front_it;
        front_bucket = center_bucket + front_it.icenter;
        assert(valid());
    }

    // ----
    // push
    // ----
private:
    //Grows number of allocated buckets towards the back of the deque
    void append_back()
    {
        //cout << "Append back: ";
        /* If the back most bucket is at the end of the
        buckets array, then we need to switch to a bigger
        buckets array.*/
        if(back_bucket == buckets + num_buckets - 1)
        {
            //cout << "creating new bucket ptr array" << endl;
            T** temp = new T*[num_buckets * 3];
            memset(temp, 0, sizeof(T*) * num_buckets * 3);
            /* temp + num_buckets will be in the middle of the new bucket ptr array.
            This will allow ample space for new buckets in the front and back.*/
            std::copy(buckets, buckets + num_buckets, temp + num_buckets);
            delete[] buckets; // Prevent mem leaks
            buckets = temp;
            //Adjust front and back bucket ptrs

            back_bucket = temp + 2 * num_buckets - 1;//front_bucket + num_buckets - 1;

            center_bucket = back_bucket - num_buckets / 2;
            front_bucket = center_bucket + front_it.icenter;

            num_buckets *= 3;
        }
        /*If we have space in the bucket ptr array and the bucket ahead of the
          current one is not allocated yet, just allocate new bucket element
        space and update the bucket ptr array. Also, set back element ptr to front
        of that new bucket space.*/
        if(*(back_bucket + 1) == nullptr)
        {
            //cout << "allocating space for another bucket in back." << endl;
            T* new_bucket = _a.allocate(ele_per_bucket);//Allocate new bucket
            *(back_bucket + 1) = new_bucket;//Set bucket ptr in front of current frontmost bucket to this one
        }
        //Update the back iterator.
        ++back_bucket;
        ++back_it.icenter;
        back_it.index = 0;
        back_it.element = *(center_bucket + back_it.icenter);
        //cout << back_it.element << endl;
    }
    /**
     * Inserts a value at the back of the deque.
       @param value to insert into the deque.
     */
public:
    //Note, back_it will point past the end element
    void push_back (const_reference v) {
        //cout << "Pushing " << v << " to the back!" << endl;
        //cout << v << " at " << &*back_it << " : index " << back_it.index << endl;
        _a.construct(&*back_it, v); //Construct the element there
        ++back_it;
        if(back_it.index == -1)//If back iterator is twice behind the last position in the back most bucket...
        {
            append_back();
            // cout << "Difference between back bucket and center bucket is now " << center_bucket - back_bucket <<endl;
            //cout << "Index now " << back_it.index << " of bucket " << back_it.icenter << endl;
        }
        ++_size; //increase size by one
        assert(valid());
    }

private:
    //Grows the number of buckets allocated in the front of the deque
    void append_front()
    {
        //cout << "Append front: ";
        /* If the front most bucket is at the start of the
        buckets array, then we need to switch to a bigger
        buckets array.*/
        if(front_bucket == buckets)
        {
            //cout << "creating new bucket ptr array" << endl;
            T** temp = new T*[num_buckets * 3];
            memset(temp, 0, sizeof(T*) * num_buckets * 3);
            /* temp + num_buckets will be in the middle of the new bucket ptr array.
            This will allow ample space for new buckets in the front and back.*/
            std::copy(buckets, buckets + num_buckets, temp + num_buckets);
            delete[] buckets; // Prevent mem leaks
            buckets = temp;
            //Adjust front and back bucket ptrs
            front_bucket = temp + num_buckets;
            center_bucket = front_bucket + num_buckets / 2;
            back_bucket = center_bucket + back_it.icenter;
            center_bucket = front_bucket + num_buckets / 2;
            num_buckets *= 3;
        }
        /*If we have space in the bucket ptr array and the bucket ahead of the
          current one is not allocated yet, just allocate new bucket element
        space and update the bucket ptr array. Also, set front element ptr to back
        of that new bucket space.*/
        if(*(front_bucket - 1) == nullptr)
        {
            //cout << "allocating space for another bucket in front." << endl;
            T* new_bucket = _a.allocate(ele_per_bucket);//Allocate new bucket
            *(front_bucket - 1) = new_bucket;//Set bucket ptr in front of current frontmost bucket to this one
        }
        //Update the front iterator.
        --front_bucket;
        --front_it.icenter;
        front_it.index = ele_per_bucket - 1;
        front_it.element = *(center_bucket + front_it.icenter) + front_it.index;
    }
public:
    /**
     * Inserts a value at the front of the deque.
       @param value to insert into the deque.
     */
    void push_front (const_reference v) {
        --front_it; //Move front closer to the beginning of its bucket
        if(front_it.index == -1)//If front iterator is ahead of the fisrt position in the front most bucket...
        {
            append_front();
            //cout << "Difference between front bucket and center bucket is now " << center_bucket - front_bucket <<endl;
        }
        //cout << "Pushing " << v << " to the front!" << endl;
        //cout << v << " at " << &*front_it << endl;
        _a.construct(&*front_it, v); //Construct the element there

        ++_size; //increase size by one
        assert(valid());
    }


    // -----
    // erase
    // -----

    /**
     * Erases element pointed by the param iterator.
     * @param iterator pointing to an element to be erased
     */
    iterator erase (iterator it) {
        //If it is begin() or end(), just do a normal pop
        if(it == front_it)
            pop_front();
        else if(it == back_it)
            pop_back();
        else//If erasing in the middle, shift everything forward. (by element)
        {
            for(iterator i = it; i != back_it - 1;  ++i)
                *i = *(i + 1);
            pop_back();
        }
        assert(valid());
        return iterator();
    }

    // ------
    // resize
    // ------

    /**
     * Sets the size of deque, effectively growing or curtailing the deque
     * @param new size of the deque.
     */
    void resize (size_type s) {
        resize(s, value_type());
        assert(valid());
    }

    /**
     * Sets the size of deque, effectively growing or curtailing the deque
     * @param new size of the deque.
       @param value to fill new elements in the case that the deque grows in size
     */
    void resize (size_type s, const_reference v) {
        //If s is greater than size, than just push back v a bunch

        if(s > size())
        {
            size_type count = s - size();
            for(int i = 0; i < count; ++i)
                push_back(v);
        }
        //Do the opposite if s is less than size
        else if(s < size())
        {
            size_type count = size() - s;
            for(int i = 0; i < count; ++i)
                pop_back();
        }
        //do nothing if s == size
        assert(valid());
    }

    // ----
    // size
    // ----

    /**
     * returns the size of the deque.
     */
    size_type size () const {
        return _size;
    }

    // ----
    // swap
    // ----

    /**
     * Swaps the elements with another deque.
       @param a deque to swap elements with.
     */
    void swap (my_deque& that) {
        my_deque& less = size() < that.size()
                         ? *this : that;
        my_deque& more = &less == this ?
                         that : *this;
        /* Swap the elements of the larger and
        smaller deques individually.*/
        for(int i = 0; i < less.size(); ++i)
        {
            T temp = move(less.at(i));
            less.at(i) = move(more.at(i));
            more.at(i) = move(temp);
        }
        size_type s = less.size();
        /*Simply push_back the excess elements the larger
          deque has into the smaller one.*/
        for(int i = s; i < more.size(); ++i)
        {
            less.push_back(more.at(i));
        }
        /*Get rid of the excess elements the previous larger one had
        Note, this doesn't do anything in the event that the
        deques have the same size*/
        more.resize(s);
        assert(valid());
    }
};

#endif // Deque_h
